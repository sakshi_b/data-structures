#include<iostream>
using namespace std;

class node
{
    public:
        int val;
        node *next;

        node(int el, node *n=0)
        {
            val=el;
            next=n;
        }
};

class list
{
    node *top;
    int s;

    public:
    list()
    {
        top=0;
        s=0;
    }

    void push(int el)
    {
        if(top==0)
            top=new node(el);

        else
        {
            top->next=new node(el,top);
        }
        s++;
    }
    
    int pop()
    {
        node *temp;
        if(s==0)
        {
            cout<<"underflow";
            return 0;
        }
        else
        {
            node *temp=top;
            top=top->next;
            delete temp;
            s--;
            return 1;
        }
    }
    void print()
    {
        node *temp=top;
        while(s>0)
        {
            cout<<temp->val;
            temp=temp->next;
            s--;

        }
    }

};

int main()
{
    int no,c;
    char ans;
    list a;
    cout<<"Menu\n";
    cout<<"1. To push an element\n";
    cout<<"2. To pop an element\n";
    cout<<"3. Print\n";
    do
    {
        cout<<"Enter choice\n";
        cin>>c;
        switch(c)
        {
            case 1: cout<<"enter no: ";
                    cin>>no;
                    a.push(no);
                    break;
            case 2:a.pop();
                   break;
            case 3:a.print();
                   break;
            default: cout<<"Wrong input\n";
        }
        cout<<"Do you wish to continue?\n";
        cin>>ans;
    }while(ans!='n');
    return 0;
}
